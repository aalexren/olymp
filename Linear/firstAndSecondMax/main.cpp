#include <iostream>
#include <vector>

using namespace std;
using ll = long long;

int n; ll a,b,cur;
ll mod = 1791791791;
ll im1 = 0;
ll im2 = 1;

int main() {

  cin >> n;
  cin >> cur >> a >> b;
  vector<ll> nums(n);
    
  nums[0] = (cur * a + b) % mod;
  for (ll i = 1; i < n; ++i) {
    nums[i] = (nums[i-1] * a + b) % mod;
  }
  
  if (nums[im1] < nums[im2]) swap(im1, im2);
  
  for (ll i = 2; i < n; ++i) {
    if (nums[im1] < nums[i]) {
      im2 = im1;
      im1 = i;
    }
    else if (nums[im2] < nums[i]) im2 = i;
  }

  cout << im1 + 1 << " " << im2 + 1;

  return 0;
}
