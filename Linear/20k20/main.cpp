#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int check(int n, vector<int> dig) {
  int o;
  while(n > 0) {
    o = n%10;
    n /= 10;
    if(dig[o]) dig[o] = 0;
    else return 0;
  }

  return 1;
}

int main() {
  bst;

  ll n; cin >> n;
  if (n > 98765431) { cout << -1; return 0; }
  vector<int> dig{0, 1, 0, 1, 1, 1, 1, 1, 1, 1};
  vector<int> num;

  while(n < 98765431) {
    n++;
    if(check(n, dig)) { cout << n; return 0;}
  }

  cout << -1;
    
  return 0;
}
