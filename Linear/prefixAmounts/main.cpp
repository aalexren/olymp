#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int main() {
  bst;
  
  int n,q;
  cin >> n >> q;
  vector<ll> nums(n);
  vector<ll> prefix(n+1);
  
  prefix[0] = 0;
  for(int i = 0; i < n; ++i) {
    cin >> nums[i];
    prefix[i + 1] = prefix[i] + nums[i];
  }

  for(int i = 0; i < q; ++i) {
    int l,r;
    cin >> l >> r;
    cout << prefix[r] - prefix[l - 1] << "\n";
  }

  return 0;
}
