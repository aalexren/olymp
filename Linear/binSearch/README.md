# Бинарный поиск

В этой задаче требуется выяснить, сколько шагов сделает бинарный поиск, если массив состоит только из нулей и единиц и необходимо найти индекс первой единицы.

**Входные данные:**

В первой строке входных данных записано два целых числа $`n,m (1 \leq n, m \leq 10^5)`$\- размер массива и количество запросов. В следующих m*m* строках идут сами запросы. 

Каждый запрос описывается единственным числом $`k(0 \leq k \leq n)`$ - позицией первой единицы в массиве. Массив нумеруется с нуля. В частности, $`k = 0`$ обозначает, что весь массив состоит целиком из единиц, а $`k = n`$ обозначает, что массив состоит целиком из нулей.

**Выходные данные:**

На каждый запрос в отдельной строке выведите единственное число - количество итераций бинарного поиска.

---

**Sample Input:**

```с++
10 3
0
5
10
```

---

**Sample Output:**

```с++
3
3
4
```
