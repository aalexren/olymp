#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int getFirstOne(int n, int l, int r, int k, int f) {
  l = -1, r = n;
  int counter = 0;
  while (l+1 < r) {
    int mid = (l+r)/2;
    if (mid < k)
      l = mid;
    else
      r = mid;
    counter++;
  }

  return counter;
}

int main() {
  bst;

  int n, m; // size, queries
  cin >> n >> m;
  vector<int> v(n);
  for (int i = 0; i < m; ++i) {
    int k; cin >> k;
    //v.assign(n, 1);
    //for (int j = 0; j < k; ++j) v[j] = 0;
    //for (int r = 0; r < v.size(); ++r) cout << v[r] << " ";
    //cout << "\n";
    cout << getFirstOne(n, -1, n, k, 1) << "\n";
  }

  return 0;
}