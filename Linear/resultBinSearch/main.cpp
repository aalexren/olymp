#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int getFirstOne(int n, int &l, int &r, int mid) {
    mid = mid;
    if (mid == 0)
      l = (l+r)/2;
    else
      r = (l+r)/2;

  return r;
}

int main() {
  bst;
  
  int n, m; // size, queries
  cin >> n >> m;
  
  int mid; int r = n; int l = -1;
  for (int i = 0; i < m; ++i) {
    cin >> mid;
    getFirstOne(n, l, r, mid);
  }
  cout << r;

  return 0;
}
// commit