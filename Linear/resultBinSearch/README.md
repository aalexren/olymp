# Результат бинарного поиска

В этой задаче вам даны ответы на запросы бинпоиска - массив, каждое значение которого совпадает с очередным `a[mid]` в реализации бинарного поиска из лекции.

```c++
int getFirstOne(vector<int> v) {
  int l = -1, r = v.size();
  while (l+1 < r) {
    int mid = (l+r)/2;
    if (v[mid] == 0)
      l = mid;
    else
      r = mid;
  }

  return r;
}
```



Вам требуется вывести результат, который получил такой бинарный поиск - позицию первой единицы. Обратите внимание - как и в предыдущей задаче, массив нумеруется с нуля.

**Входные данные**

В первой строке входных данных записано два целых числа $`n,m(1 \leq n \leq 10^5; 1 \leq m \leq 20)`$ - размер массива и количество итераций бинарного поиска, которое произошло при запуске бинарного поиска на загаданном массиве.

В следующей строке записано m*m* чисел, каждое из которых совпадает с $`0`$ либо с $`1`$ - значения `a[mid]` для очередных запросов в бинарном поиске. Гарантируется, что таким значениям соответствует корректный массив.

**Выходные данные**

Выведите одно число - позицию первой единицы в массиве. Если в массиве не оказалось единиц, выведите число $`n`$.

---

**Sample Input 1:**

```c++
10 4
1 0 0 0
```

---

**Sample Output 1:**

```c++
4
```

---

**Sample Input 2:**

```c++
10 3
1 1 1
```

---

**Sample Output 2:**

```c++
0
```

---

**Sample Input 3:**

```c++
10 4
0 0 0 0
```

---

**Sample Output 3:**

```c++
10
```

