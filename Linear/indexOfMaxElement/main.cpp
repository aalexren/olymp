#include <iostream>
#include <vector>

using namespace std;

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  cout.tie(0);

  int n; cin >> n;
  int index_max;
  vector<int> nums(n);
  for (int i = 0; i < n; ++i) {
    cin >> nums[i];
  }

  index_max = 0;
  for (int i = 0; i < n; ++i) {
    if (nums[index_max] < nums[i]) index_max = i;
  }
  cout << index_max + 1;

  return 0;
}
