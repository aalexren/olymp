#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

int main() {
  bst;

  int n; cin >> n;
  vector<ll> nums(n);
  vector<ll> prefix(n+1);

  prefix[0] = 0;
  ll index_min = 0; int negatives = 0;
  for(int i = 0; i < n; ++i) {
    cin >> nums[i];
    prefix[i+1] = nums[i] + prefix[i];
    if (nums[i] < 0) negatives++;
  }

  if (negatives == n) {
    ll minn = nums[0];
    for(int i = 0; i < n; i++) {
      minn = max(minn, nums[i]);
    }
    cout << minn; return 0;
  }

  ll maxx = prefix[1];
  for(int r = 1; r < n+1; ++r) {
    if (prefix[index_min] > prefix[r]) index_min = r;
    maxx = max(maxx, prefix[r] - prefix[index_min]);
  }

  cout << maxx;

  return 0;
}
