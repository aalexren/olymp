#include <stdio.h>

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

using namespace std;
using ll = long long;
#define bst           \
   cin.tie(nullptr);  \
   cout.tie(nullptr); \
   ios_base::sync_with_stdio(false);

struct dsu {
   vector<int> leader;
   vector<int> size;  // not a rank, exactly size
   vector<int> sum;
   int max_;

   void make(int ind, int i) {
      size[ind] = 1;
      sum[ind] += i;
      max_ = max(max_, sum[ind]);
      return;
   }

   int find(int i) {
      if (leader[i] != i) leader[i] = find(leader[i]);
      return leader[i];
   }

   void join(int x, int y) {
      int x_id = find(x);
      int y_id = find(y);

      /* already joined */
      if (x_id == y_id) return;

      if (size[x_id] > size[y_id]) {
         leader[y_id] = x_id;
         size[x_id] += size[y_id];
         sum[x_id] += sum[y_id];
         max_ = max(sum[x_id], max_);
      } 
      else {
         leader[x_id] = y_id;
         size[y_id] += size[x_id];
         sum[y_id] += sum[x_id];
         max_ = max(sum[y_id], max_);
         if (size[x_id] == size[y_id])
            size[y_id]++;
      }

      return;
   }

   dsu(int n) {
      n++;
      leader.resize(n);
      size.resize(n);
      sum.resize(n);
      for (int i = 0; i < n; ++i) {
         leader[i] = i;
         size[i] = 1;
         sum[i] = 0;
      }
      max_ = sum[0];
   }
};

int main() {
   bst;
   int n, m, u, v;
   scanf("%d %d", &n, &m);
   dsu d(n);

   for (int i = 0; i < n; ++i) {
      int temp; scanf("%d", &temp);
      d.make(i+1, temp);
   }

   for (int i = 0; i < m; i++) {
      scanf("%d%d", &u, &v);
      d.join(u, v);
      printf("%d\n", d.max_);
   }

   return 0;
}