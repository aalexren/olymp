#include <stdio.h>

#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

using namespace std;
using ll = long long;
#define bst           \
   cin.tie(nullptr);  \
   cout.tie(nullptr); \
   ios_base::sync_with_stdio(false);

struct dsu {
   vector<int> leader;
   vector<int> size;  // not a rank, exactly size
   vector<int> sum;

   int find(int i) {
      if (leader[i] != i) leader[i] = find(leader[i]);
      return leader[i];
   }

   void join(int x, int y) {
      int x_id = find(x);
      int y_id = find(y);

      /* already joined */
      if (x_id == y_id) return;

      if (size[x_id] > size[y_id]) {
         leader[y_id] = x_id;
         size[x_id] += size[y_id];
         sum[x_id] += sum[y_id];
      } 
      else {
         leader[x_id] = y_id;
         size[y_id] += size[x_id];
         sum[y_id] += sum[x_id];
         if (size[x_id] == size[y_id])
            size[y_id]++;
      }

      return;
   }

   dsu(int n) {
      n++;
      leader.resize(n);
      size.resize(n);
      sum.resize(n);
      for (int i = 0; i < n; ++i) {
         leader[i] = i;
         size[i] = 1;
         sum[i] = 0;
      }
   }
};

int main() {
   bst;
   int n, m, q, u, v;
   scanf("%d %d %d", &n, &m, &q);
   dsu d(n);

   for (int i = 0; i < m; ++i) {
      scanf("%d%d", &u, &v);
      d.join(u, v);
   }
   for (int i = 0; i < q; ++i) {
      scanf("%d%d", &u, &v);
      if (d.find(u) == d.find(v)) { printf("1"); break; }
      else printf("0");
   }

   return 0;
}