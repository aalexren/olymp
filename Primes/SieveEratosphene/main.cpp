#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <algorithm>
#include <stack>
#include <queue>
#include <set>
#include <ctime>

using namespace std;
using ll = long long;
#define bst cin.tie(nullptr); cout.tie(nullptr); ios_base::sync_with_stdio(false);

vector<bool> sieve(ll n) {
    vector<bool> v(n+1, true);
    v[0] = v[1] = false;
    
    for (ll i = 2; i * i <= n; ++i) {
        if (v[i])
        for (ll j = i * i; j <= n; j+=i) {
            v[j] = false;
        }
    }
    
    return v;
}

int main() {
  bst;

  auto v = sieve(20);
  for(ll i = 2; i < 20; i++) {
    if (v[i]) cout << i << " ";
  }

  return 0;
}
